//
//  ZHTopicTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/8.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHTopicTableViewCell.h"

@interface ZHTopicTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *activeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *joinNum;
@property (weak, nonatomic) IBOutlet UILabel *stopTime;



@end
@implementation ZHTopicTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.headerImage.image = [UIImage imageNamed:@"1"];
    self.icon.image = [UIImage imageNamed:@"1"];
    self.nameLabel.text = @"张晓明";
    self.activeLabel.text = @"去爬山";
    self.joinNum.text = @"5人";
    self.stopTime.text = @"截止日期2016";
}

//评论按钮
- (IBAction)wantJoin:(id)sender {
    
    if (self.delegate) {
       [self.delegate tableViewCell:self didClickedLinkWithData:sender];
    }
    

}

@end
