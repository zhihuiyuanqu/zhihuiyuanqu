//
//  ZHNeighborJoinTableViewCell.m
//  
//
//  Created by leo on 16/4/7.
//
//

#import "ZHNeighborJoinTableViewCell.h"

@interface ZHNeighborJoinTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *icon;//动态图片
@property (weak, nonatomic) IBOutlet UILabel *journalLabel;//动态说说
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;//发起人名
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;//地点
@property (weak, nonatomic) IBOutlet UILabel *startLabel;//发起日期
@property (weak, nonatomic) IBOutlet UILabel *stopLabel;//截止日期
@property (weak, nonatomic) IBOutlet UILabel *activeLabel;//活动介绍
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;//参加人数

@property (weak, nonatomic) IBOutlet UIButton *joinButton;//参加按钮


@end
@implementation ZHNeighborJoinTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.icon.image = [UIImage imageNamed:@"1"];
    self.journalLabel.text = @"周末去爬山";
    self.nameLabel.text = @"张晓明";
    self.placeLabel.text = @"大蜀山";
    self.startLabel.text = @"发起日期";
    self.stopLabel.text = @"截止日期";
    self.activeLabel.text = @"";
    self.numberLabel.text = @"5人";
    
}
- (IBAction)joinButton:(id)sender {
    if ([self.joinButton.titleLabel.text isEqualToString:@"我要参加"]) {
        [self.joinButton setTitle:@"退出活动" forState:UIControlStateNormal];
        self.numberLabel.text = @"6人";
    }else{
        [self.joinButton setTitle:@"我要参加" forState:UIControlStateNormal];
        self.numberLabel.text = @"5人";
    }
    
    
}

@end
