//
//  ZHNeighborTableViewCell.h
//  
//
//  Created by leo on 16/4/7.
//
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHNeighborTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度
@end
