//
//  ZHNeighborTableViewCell.m
//  
//
//  Created by leo on 16/4/7.
//
//

#import "ZHNeighborTableViewCell.h"

@interface ZHNeighborTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *imagePerson;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *journal;

@property (strong, nonatomic) IBOutlet UIImageView *icon;

@property (strong, nonatomic)UILabel *stopTime;




@end
@implementation ZHNeighborTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.imagePerson.image = [UIImage imageNamed:@"1"];
    self.name.text = @"张晓明";
    
    NSString *mes = @"在viewcontroller声明的IBAction可以连接到button 上，但是自己新添加的两个view controller新类里面声明的IBAction都不可以连接到Button，这怎么解决？我在看iOS6 in practice，这是书里面一个处理照片的那个程序PicDecor。111111111111111111111111111111111111111111111111111111";
    CGSize size = CGSizeMake(SCREEN_WIDTH-30-CGRectGetMaxX(self.imagePerson.frame), CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:14]
                          };
    
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.journal.text = mes;
    self.journal.frame = CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.name.frame), SCREEN_WIDTH-30-CGRectGetMaxX(self.imagePerson.frame), labelSize.height);
    
    float width = (SCREEN_WIDTH-CGRectGetMaxX(self.imagePerson.frame)-20)/3.f-10;
    
    
    for (int i=0; i<5; i++) {
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10+(width+10)*(i%3), CGRectGetMaxY(self.journal.frame)+(width+10)*(i/3), width, width)];
        self.icon.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:self.icon];
    }
    
    NSLog(@"%f",CGRectGetMaxX(self.imagePerson.frame));
    
    self.stopTime.text = @"5分钟前";
    self.maxY = CGRectGetMaxY(self.stopTime.frame);
    
//    self.maxY = CGRectGetMaxY(self.journal.frame)+CGRectGetHeight(self.icon.frame)+CGRectGetHeight(self.stopTime.frame) +10;

}

- (UILabel *)stopTime{
    if (_stopTime==nil) {
        _stopTime = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.icon.frame)+5, SCREEN_WIDTH/2.f, 25)];
        _stopTime.textColor = [UIColor grayColor];
        _stopTime.backgroundColor = [UIColor greenColor];
        
        [self.contentView addSubview:_stopTime];
    }
    return _stopTime;
}





@end
