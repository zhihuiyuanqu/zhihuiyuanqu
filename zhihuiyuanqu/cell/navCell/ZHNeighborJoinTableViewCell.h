//
//  ZHNeighborJoinTableViewCell.h
//  
//
//  Created by leo on 16/4/7.
//
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHNeighborJoinTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;

@end
