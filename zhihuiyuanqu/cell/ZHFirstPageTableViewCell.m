//
//  ZHFirstPageTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageTableViewCell.h"

@interface ZHFirstPageTableViewCell ()
@end
@implementation ZHFirstPageTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.titleLabel.text = @"折扣广告标题";
    self.icon.image = [UIImage imageNamed:@"1"];
    self.timeLabel.text = @"5分钟前";
    
}

@end
