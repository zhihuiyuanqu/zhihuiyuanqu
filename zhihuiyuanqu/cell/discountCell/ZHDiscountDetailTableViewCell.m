//
//  ZHDiscountDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHDiscountDetailTableViewCell.h"

@interface ZHDiscountDetailTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *stopTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *touchShare;




@end
@implementation ZHDiscountDetailTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.icon.image = [UIImage imageNamed:@"1"];
    self.nameLabel.text = @"XX土菜馆";
    self.stopTimeLabel.text = @"截止日期";
    self.touchShare.text = @"点击分享到,朋友圈,微博,QQ空间,即可参加该活动。";
//    self.detailLabel.text = @"详情";
    
    NSString *mes = [NSString stringWithFormat:@"在viewcontroller声明的IBAction可以连接到button 上，但是自己新添加的两个view controller新类里面声明的IBAction都不可以连接到Button，这怎么解决？我在看iOS6 in practice，这是书里面一个处理照片的那个程序PicDecor。111111111111111111111111111111111111111111111111111111"];
    
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:15]
                          };
    
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;

    self.detailLabel.text = mes;
    self.detailLabel.frame = CGRectMake(10, CGRectGetMaxY(self.stopTimeLabel.frame), SCREEN_WIDTH-20, labelSize.height);
    self.maxY = CGRectGetMaxY(self.detailLabel.frame)+CGRectGetHeight(self.touchShare.frame) ;
    
}


@end
