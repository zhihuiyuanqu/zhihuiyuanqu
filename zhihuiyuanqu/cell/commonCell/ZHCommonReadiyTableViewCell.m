//
//  ZHCommonReadiyTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCommonReadiyTableViewCell.h"

@interface ZHCommonReadiyTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;




@end
@implementation ZHCommonReadiyTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    
    self.icon.image = [UIImage imageNamed:@"1"];
    self.timeLabel.text = @"3小时前";
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:15]
                          };
    NSString *mes = [NSString stringWithFormat:@"32楼下垃圾满了，赶紧清理"];
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.detailLabel.text = mes;
    self.detailLabel.frame = CGRectMake(10, 0, SCREEN_WIDTH-20, labelSize.height);
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

- (IBAction)deleteButton:(id)sender {
}



@end
