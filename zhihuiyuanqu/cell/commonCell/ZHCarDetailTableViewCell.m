//
//  ZHCarDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCarDetailTableViewCell.h"

@interface ZHCarDetailTableViewCell ()

@property (strong, nonatomic) IBOutlet UIView *mapView;//地图

@property (weak, nonatomic) IBOutlet UITextField *placeTextField;//选择小区
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;//共享时间
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;//理想价位
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;//联系电话

@end
@implementation ZHCarDetailTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.placeTextField.text = @"万科金域华府";
    self.timeTextField.text = @"上午8：30";
    self.moneyTextField.text = @"150/月";
    self.phoneTextField.text = @"138888888";
    self.maxY = CGRectGetMaxY(self.phoneTextField.frame)+1;
}


- (IBAction)partTextFieldAction:(UITextField *)sender {
}
- (IBAction)timeTextFieldAction:(UITextField *)sender {
}
- (IBAction)moneyTextFieldAction:(UITextField *)sender {
}

- (IBAction)phoneTextField:(UITextField *)sender {
}

- (IBAction)haveCarAction:(UIButton *)sender {
}

- (IBAction)lookCarAction:(UIButton *)sender {
}



@end
