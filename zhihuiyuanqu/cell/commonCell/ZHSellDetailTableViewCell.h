//
//  ZHSellDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;

@interface ZHSellDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;

@property(nonatomic,assign)CGFloat maxY;

@property(nonatomic,strong)NSString *tel;
@end
