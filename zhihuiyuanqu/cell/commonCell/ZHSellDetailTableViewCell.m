//
//  ZHSellDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellDetailTableViewCell.h"

@interface ZHSellDetailTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
//图片
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;//描述
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;//价位
@property (weak, nonatomic) IBOutlet UILabel *contactsLabel;//联系人
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;//联系电话
@property (nonatomic, strong) NSTimer *timer;
@end
@implementation ZHSellDetailTableViewCell

#define timer_duration 2

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.titleLabel.text = @"标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题";
    [self scrollView];
    self.detailLabel.text = @"描述描述描述描述描述";
    self.moneyLabel.text = @"800元";
    self.contactsLabel.text = @"李先生";
    self.phoneLabel.text = @"138888888";
    self.tel = self.phoneLabel.text;
    
    self.maxY = CGRectGetMaxY(self.phoneLabel.frame)+1;

}
#pragma mark 懒加载
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor redColor];
//        _scrollView.delegate = self;
        
    }
    return _scrollView;
}
- (NSTimer *)timer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:timer_duration target:self selector:@selector(runAction) userInfo:nil repeats:YES];
        [_timer setFireDate:[NSDate distantFuture]];
        
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}
@end
