//
//  ZHLookCarTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLookCarTableViewCell.h"

@interface ZHLookCarTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *duringLabel;

@end
@implementation ZHLookCarTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.icon.image = [UIImage imageNamed:@"1"];
    self.placeLabel.text = @"鼎鑫BOSS中心";
    self.timeLabel.text = @"停车时间";
    self.duringLabel.text = @"晚上6：00-早上9：00";
}

- (IBAction)chatButtonAction:(id)sender {
    
}


@end
