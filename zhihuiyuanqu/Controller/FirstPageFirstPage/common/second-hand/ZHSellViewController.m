//
//  ZHSellViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellViewController.h"

#import "ZHSellTableViewCell.h"
#import "ZHFirstPageAdsModel.h"

#import "ZHSellDetailViewController.h"

@interface ZHSellViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *sortButton;//分类按钮
@property (weak, nonatomic) IBOutlet UIButton *nearbyButton;//附近按钮

@property(nonatomic,strong)UIScrollView *sortScrollView;//分类视图
@property(nonatomic,strong)UIScrollView *nearbyScrollView;//附近视图

@property(nonatomic,strong)NSMutableArray *sortSourcedata;//分类数据源
@property(nonatomic,strong)NSMutableArray *nearbySourcedata;//附近数据源

@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

#define button_Tag 100

@implementation ZHSellViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view addSubview:self.tableView];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSellTableViewCell" owner:self options:nil].lastObject;
    }
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellDetailViewController *detail = [[ZHSellDetailViewController alloc]init];
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, SCREEN_HEIGHT-30) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSellTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}

#pragma mark 点击三种选项
- (IBAction)sortButtonAction:(UIButton *)sender {
    //判断classScrollView是否存在
    if (self.sortScrollView) {
        [self.sortScrollView removeFromSuperview];
        self.sortScrollView =nil;
        
    }else{
        self.sortScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH/2.f, SCREEN_WIDTH/2.f)];
        self.sortScrollView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:self.sortScrollView];
        for (int i=0; i<self.sortSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,30*i, SCREEN_WIDTH/2.f, 30);
            [button setTitle:self.sortSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,5, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(sortButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.sortScrollView addSubview:button];
            // 设置contentSize
            self.sortScrollView.contentSize = CGSizeMake(SCREEN_WIDTH/2.f,CGRectGetMaxY(button.frame));
        }
    }

}
- (void)sortButAction:(UIButton *)button{
    [self.sortButton setTitle:self.sortSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.sortButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.sortButton.contentEdgeInsets = UIEdgeInsetsMake(6,5, 6, 0);
    [self.sortScrollView removeFromSuperview];
    self.sortScrollView =nil;
}
- (IBAction)nearbyButtonAction:(UIButton *)sender {
    
    //判断classScrollView是否存在
    if (self.nearbyScrollView) {
        [self.nearbyScrollView removeFromSuperview];
        self.nearbyScrollView =nil;
        
    }else{
        self.nearbyScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.f, 30, SCREEN_WIDTH/2.f, SCREEN_WIDTH/2.f)];
        self.nearbyScrollView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:self.nearbyScrollView];
        for (int i=0; i<self.nearbySourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,30*i, SCREEN_WIDTH/2.f, 30);
            [button setTitle:self.nearbySourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,5, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(nearbyButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.nearbyScrollView addSubview:button];
            // 设置contentSize
            self.nearbyScrollView.contentSize = CGSizeMake(SCREEN_WIDTH/2.f,CGRectGetMaxY(button.frame));
        }
    }

}
- (void)nearbyButAction:(UIButton *)button{
    [self.nearbyButton setTitle:self.nearbySourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.nearbyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.nearbyButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.nearbyScrollView removeFromSuperview];
    self.nearbyScrollView =nil;
}
#pragma mark 数据源的懒加载
- (NSMutableArray *)sortSourcedata{
    if (_sortSourcedata==nil) {
        _sortSourcedata = @[@"美食",@"美容",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _sortSourcedata;
}
- (NSMutableArray *)nearbySourcedata{
    if (_nearbySourcedata==nil) {
        _nearbySourcedata = @[@"美食",@"",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _nearbySourcedata;
}
@end
