//
//  ZHSellDetailViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellDetailViewController.h"

#import "ZHSellDetailTableViewCell.h"
#import "ZHFirstPageAdsModel.h"

@interface ZHSellDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@property(nonatomic,strong)UIButton *phoneButton;//电话联系

@property(nonatomic,strong)UIButton *chatButton;//聊天

@property(nonatomic,strong)NSString *tel;
@end

@implementation ZHSellDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNavigationItem];
     self.tabBarController.tabBar.hidden = YES;
    [self.view addSubview:self.phoneButton];
    [self.view addSubview:self.chatButton];
    [self.view addSubview:self.tableView];
    
    [self.view bringSubviewToFront:self.phoneButton];
    [self.view bringSubviewToFront:self.chatButton];
}
#pragma mark 电话，聊天按钮
- (UIButton *)phoneButton{
    if (_phoneButton==nil) {
        _phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _phoneButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH/2.f, 40);
        _phoneButton.layer.borderWidth = 1;
        [_phoneButton setTitle:@"电话联系" forState:UIControlStateNormal];
        [_phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_phoneButton addTarget:self action:@selector(phoneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _phoneButton;
}
- (UIButton *)chatButton{
    if (_chatButton==nil) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.frame = CGRectMake(SCREEN_WIDTH/2.f, SCREEN_HEIGHT-104, SCREEN_WIDTH/2.f, 40);
        _chatButton.layer.borderWidth = 1;
        [_chatButton setTitle:@"聊天" forState:UIControlStateNormal];
        [_chatButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chatButton addTarget:self action:@selector(chatButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatButton;
}
- (void)phoneButtonAction:(UIButton *)button{

    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.tel]]];

}

- (void)chatButtonAction:(UIButton *)button{
    
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"二手详情";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:17],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
}
- (void)rightItemAction{
    
}

#pragma mark tabieView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSellDetailTableViewCell" owner:self options:nil].lastObject;
    }
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    self.tel = cell.tel;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSellDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}

@end
