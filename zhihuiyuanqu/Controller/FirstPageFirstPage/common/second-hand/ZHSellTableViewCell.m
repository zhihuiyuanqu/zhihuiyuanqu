//
//  ZHSellTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellTableViewCell.h"

@interface ZHSellTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;


@end
@implementation ZHSellTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    
    self.icon.image = [UIImage imageNamed:@"1"];
    self.titleLabel.text = @"二手物品二手物品二手物品二手物品二手物品二手物品二手物品";
    self.placeLabel.text = @"合肥市.蜀山区";
    self.timeLabel.text = @"2016-4-11 15:20";
    self.moneyLabel.text = @"价格：800元";
    
}

@end
