//
//  ZHLookCarViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLookCarViewController.h"

#import "ZHLookCarTableViewCell.h"
#import "ZHFirstPageAdsModel.h"
@interface ZHLookCarViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating>

/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSMutableArray *dataSource;

@property(nonatomic,strong)UISearchController *search;

@end

@implementation ZHLookCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view addSubview:self.tableView];
}


#pragma mark searchController的协议方法
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHLookCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHLookCarTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.search.searchBar;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHLookCarTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (UISearchController *)search{
    if (!_search) {
        
        _search = [[UISearchController alloc]initWithSearchResultsController:nil];
        
        _search.searchResultsUpdater = self;
        _search.dimsBackgroundDuringPresentation = YES;
        _search.searchBar.placeholder = @"输入小区名称";
        

    }
    return _search;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}
@end
