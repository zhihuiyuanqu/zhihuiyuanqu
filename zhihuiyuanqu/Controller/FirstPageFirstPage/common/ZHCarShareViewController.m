//
//  ZHCarShareViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCarShareViewController.h"

#import "ZHHaveCarViewController.h"
#import "ZHLookCarViewController.h"

#import "ZHCarDetailViewController.h"

@interface ZHCarShareViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *titleScrollView;

@property(nonatomic,strong)UIScrollView *contentScrollView;

@property(nonatomic,strong)NSMutableArray *buttons;

@end

@implementation ZHCarShareViewController

#define titleButton_Tag 100

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"车位共享";
    self.tabBarController.tabBar.hidden = YES;

    [self setUpNavigationItem];
    [self.view addSubview:self.titleScrollView];
    [self.view addSubview:self.contentScrollView];
    
    [self addChildViewController];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:17],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
}


- (void)rightItemAction{
    
    ZHCarDetailViewController *carDetail = [[ZHCarDetailViewController alloc]init];
    
    [self.navigationController pushViewController:carDetail animated:YES];
}
- (void)addChildViewController{
    ZHHaveCarViewController *haveCar = [[ZHHaveCarViewController alloc]init];
    [self addChildViewController:haveCar];
    
    ZHLookCarViewController *lookCar = [[ZHLookCarViewController alloc]init];
    [self addChildViewController:lookCar];
}
#pragma mark titleButton
- (void)createTitleButton{
    NSArray *titles = @[@"我有车位",@"我找车位"];
    for (int i=0; i<titles.count; i++) {
        UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        titleButton.frame = CGRectMake(SCREEN_WIDTH/2.f*i, 0, SCREEN_WIDTH/2.f,30);
        if (i==0) {
            [titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }else{
            [titleButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
        
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
        
        titleButton.tag = i+titleButton_Tag;
        [titleButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.titleScrollView addSubview:titleButton];
        [self.buttons addObject:titleButton];
    }
}
- (void)buttonAction:(UIButton *)button{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (button.tag==titleButton_Tag+0) {
        
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }else{
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
    }
    [self setUpOneChildViewController:button.tag-titleButton_Tag];
    CGPoint point = self.titleScrollView.contentOffset ;
    point.x = SCREEN_WIDTH*(button.tag-titleButton_Tag);
    [self.contentScrollView setContentOffset:point animated:NO];
}
- (void)setUpOneChildViewController:(NSUInteger)i
{
    
    UIViewController *vc = self.childViewControllers[i];
    
    if (vc.view.superview) {
        return;
    }
    vc.view.frame = CGRectMake(i*SCREEN_WIDTH, 0,SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.titleScrollView.frame));
    
    [self.contentScrollView addSubview:vc.view];
    
}
#pragma mark UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger tage = self.contentScrollView.contentOffset.x/SCREEN_WIDTH;
    [self buttonAction:self.buttons[tage]];
    [self.buttons[tage] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (tage==0) {
        
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }else{
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
    }
    [self setUpOneChildViewController:tage];
    
}
#pragma mark 懒加载
- (UIScrollView *)titleScrollView{
    if (!_titleScrollView) {
        _titleScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,30)];
        _titleScrollView.showsHorizontalScrollIndicator = YES;
        _titleScrollView.delegate = self;
        
        _titleScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
        [self createTitleButton];
    }
    return _titleScrollView;
}
- (UIScrollView *)contentScrollView{
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScrollView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.titleScrollView.frame))];
        
        _contentScrollView.pagingEnabled = YES;
        _contentScrollView.delegate = self;

        _contentScrollView.contentOffset = CGPointMake(0, CGRectGetMaxY(self.titleScrollView.frame));
        _contentScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*2, CGRectGetMaxY(self.titleScrollView.frame));
    }
    return _contentScrollView;
}
- (NSMutableArray *)buttons{
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}
- (void)viewWillAppear:(BOOL)animated{
    [self setUpOneChildViewController:0];
}
- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
