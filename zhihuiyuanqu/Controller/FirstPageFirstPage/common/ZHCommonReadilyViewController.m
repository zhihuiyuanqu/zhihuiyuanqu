//
//  ZHCommonReadilyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCommonReadilyViewController.h"

#import "ZHCommonReadiyTableViewCell.h"
#import "ZHFirstPageAdsModel.h"
@interface ZHCommonReadilyViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHCommonReadilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"随手拍";
    
    self.tabBarController.tabBar.hidden = YES;
    
    [self setUpNavigationItem];
    
    [self.view addSubview:self.tableView];
}
- (void)setUpNavigationItem{
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:17],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    //两侧按钮颜色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCommonReadiyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCommonReadiyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHCommonReadiyTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHCommonReadiyTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}
- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
