//
//  ZHFirstPageFirstPageViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageFirstPageViewController.h"

#import "ZHFirstPageFirstPageHeaderView.h"
#import "ZHFirstPageTableViewCell.h"

#import "ZHFirstPageAdsModel.h"
#import "ZHFirstPageCommonModel.h"

#import "ZHCommonReadilyViewController.h"
#import "ZHCarShareViewController.h"
#import "ZHSecondHandViewController.h"

@interface ZHFirstPageFirstPageViewController ()<UITableViewDelegate,UITableViewDataSource>

/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSMutableArray *dataSource;

/** 头视图, 包含广告和功能区 */
@property (nonatomic, strong)ZHFirstPageFirstPageHeaderView  *headerView;
@end

@implementation ZHFirstPageFirstPageViewController

//广告栏
#define PATH_ADSURL @""

//功能区
#define PATH_COMMONURL @""

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    
    [self requestData];//请求数据
}
#pragma mark 请求数据
- (void)requestData{
    
    [ZHRequestData getWithURL:PATH_ADSURL params:nil success:^(id json) {
        [self adsData:json];
    } failure:^(NSError *error) {
        [self adsData:error];
        NSLog(@"网络请求失败，原因%@",error);
    }];
    
    [ZHRequestData getWithURL:PATH_COMMONURL params:nil success:^(id json) {
        [self adsData:json];
    } failure:^(NSError *error) {
        [self commonData:error];
        NSLog(@"网络请求失败，原因%@",error);
        [self stopRefresh];
    }];
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
}
- (void)adsData:(NSDictionary *)data{
    // 1. 获取存放所有广告信息的数组
//    NSArray *allAds = data[@"data"][@"banner"][@"data"];
//    
//    // 2. 遍历数组, 把字典转成模型
    NSMutableArray *ads = [[NSMutableArray alloc] init];
//
//    for (NSDictionary *info in allAds) {
//        // 字典转模型
//        ZHFirstPageAdsModel *model = [[ZHFirstPageAdsModel alloc] initWithDictionary:info error:nil];
//        
//        // 添加到数组中做存储
//        [ads addObject:model];
//    }
    ads = @[@"",@"",@"",@"",@""];
    // 3. 把数据给headerView做界面展示
    [self.headerView setAdsData:ads clickCallBack:^(ZHFirstPageAdsModel *model) {
        NSLog(@"111");
        
    }];
}
- (void)commonData:(NSDictionary *)data{
    // 1. 获取存放所有功能显示模块相关的数据
//    NSArray *allData = data[@"data"][@"category"][@"data"];
    
    // 临时存储所有的模型
    NSMutableArray *allModels = [NSMutableArray array];
    
    // 2. 遍历数组, 把所有的字典, 转成需要的模型
//    for (NSDictionary *info in allData) {
//        
//        // 字典转模型
//        ZHFirstPageCommonModel *model = [[ZHFirstPageCommonModel alloc] initWithDictionary:info error:nil];
//        
//        [allModels addObject:model];
//    }
    allModels = @[@"",@"",@"",@"",@"",@"",@"",@""];
    
    __weak ZHFirstPageFirstPageViewController *weakSelf = self;
    // 设置所有的数据, 并设置点击的回调
    [self.headerView setCommonData:allModels clickCallBacl:^(NSInteger integer) {
        switch (integer) {
            case 0:{
                [weakSelf.navigationController pushViewController:[[ZHCarShareViewController alloc]init] animated:YES];
                break;
            }
            case 2:{
                
                // 这里是点击分类的回调
                [weakSelf.navigationController pushViewController:[[ZHSecondHandViewController alloc]init] animated:YES];
                break;
            }
            case 5:{
                
                // 这里是点击分类的回调
                [weakSelf.navigationController pushViewController:[[ZHCommonReadilyViewController alloc]init] animated:YES];
                break;
            }
            default:
                break;
        }
        
    }];

}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    
    self.tableView.mj_header = header;
    
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.dataSource.count;
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHFirstPageTableViewCell" owner:self options:nil].lastObject;
    }
    cell.model = self.dataSource[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // 头视图
        _tableView.tableHeaderView = self.headerView;
        
        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHFirstPageTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (ZHFirstPageFirstPageHeaderView *)headerView{
    if (_headerView==nil) {
        _headerView = [[ZHFirstPageFirstPageHeaderView alloc]init];

    }
    return _headerView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
//        _dataSource = [[NSMutableArray alloc] init];
        _dataSource = @[@"",@"",@"",@"",@"",@"",@"",@""];
    }
    
    return _dataSource;
}
@end
