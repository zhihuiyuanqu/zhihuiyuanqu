//
//  ZHNeighborJoinViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/7.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNeighborJoinViewController.h"

#import "ZHNeighborJoinTableViewCell.h"

#import "ZHFirstPageAdsModel.h"

@interface ZHNeighborJoinViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation ZHNeighborJoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //隐藏tabBar
    self.tabBarController.tabBar.hidden = YES;
    
    [self.view addSubview:self.tableView];
    
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 750;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNeighborJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHNeighborJoinTableViewCell" owner:self options:nil].lastObject;
    }
//    ZHFirstPageAdsModel *model = self.dataSource[indexPath.row];
//    cell.model = model;
    return cell;
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHNeighborJoinTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
#pragma mark 我要参加按钮


- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.tabBarController.tabBar.hidden = NO;
}
@end
