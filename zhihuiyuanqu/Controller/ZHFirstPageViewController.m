//
//  ZHFirstPageViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageViewController.h"
#import "CCLocationManager.h"
#import "ZHFirstPageNavLocationViewController.h"
#import "ZHFirstPageNavAddActiveViewController.h"

#import "ZHFirstPageFirstPageViewController.h"
#import "ZHFirstPageTopicViewController.h"
#import "ZHFirstPageDiscountViewController.h"

#define Title_Tag 100

@interface ZHFirstPageViewController ()<UIScrollViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationmanager;
    
}


@property(nonatomic,strong)UIScrollView *titleScrollView;

@property(nonatomic,strong)UIScrollView *HScrollView;

@property(nonatomic,strong)UIScrollView *contentScrollView;

@property(nonatomic,strong)NSMutableArray *buttons;

@property(nonatomic,strong)UILabel *label1;
@property(nonatomic,strong)UILabel *label2;
@property(nonatomic,strong)UILabel *label3;

@property(nonatomic,strong)NSMutableArray *labels;
@end

@implementation ZHFirstPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //定位功能
    [UIApplication sharedApplication].idleTimerDisabled = TRUE;
    locationmanager = [[CLLocationManager alloc] init];
    [locationmanager requestAlwaysAuthorization];        //NSLocationAlwaysUsageDescription
    [locationmanager requestWhenInUseAuthorization];     //NSLocationWhenInUseDescription
    locationmanager.delegate = self;
    
    //导航栏左右视图
    [self setUpNavigationItem];
    
    [self.view addSubview:self.titleScrollView];
    [self.view addSubview:self.HScrollView];
    [self.view addSubview:self.contentScrollView];
    
    [self addContentChildViewController];

}
#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    //导航栏背景颜色
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
    
    //两侧按钮颜色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(leftItemAction)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
}
- (void)leftItemAction{
    
    //纯代码跳到storyboard里面的viewController
    UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
    
    ZHBaseNavViewController *baseNav = [story instantiateViewControllerWithIdentifier:@"location"];
    
    [self.navigationController presentViewController:baseNav animated:YES completion:nil];
}
- (void)rightItemAction{
    
    UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];

    ZHBaseNavViewController *baseNav = [[ZHBaseNavViewController alloc]initWithRootViewController:[story instantiateViewControllerWithIdentifier:@"ZHFirstPageNavAddActiveViewController"]];
    
    [self.navigationController presentViewController:baseNav animated:YES completion:nil];
}
- (void)addContentChildViewController{
     ZHFirstPageFirstPageViewController *firstPage = [[ZHFirstPageFirstPageViewController alloc]init];
    firstPage.title = @"首页";
    [self addChildViewController:firstPage];
    
    ZHFirstPageDiscountViewController *discount = [self.storyboard instantiateViewControllerWithIdentifier:@"ZHFirstPageDiscountViewController"];
    discount.title = @"折扣";
    [self addChildViewController:discount];

    ZHFirstPageTopicViewController *topic = [[ZHFirstPageTopicViewController alloc]init];
    topic.title = @"话题";
    [self addChildViewController:topic];
}
#pragma mark titleScorllView
- (UIScrollView *)titleScrollView{
    
    if (_titleScrollView==nil) {
        _titleScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, 40)];
//        _titleScrollView.showsHorizontalScrollIndicator = YES;
        
        _titleScrollView.delegate = self;

        _titleScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
        [self createTitleButton];
    }
    return _titleScrollView;
}
- (UIScrollView *)HScrollView{
    if (_HScrollView==nil) {
        _HScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScrollView.frame), SCREEN_WIDTH, 1)];
        _HScrollView.delegate = self;
        
        _HScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
        
        [_HScrollView addSubview:self.label1];
        [_HScrollView addSubview:self.label2];
        [_HScrollView addSubview:self.label3];
    }
    return _HScrollView;
}


- (void)createTitleButton{
    
    NSArray *titles = @[@"首页",@"折扣",@"活动"];
    for (int i=0; i<3; i++) {
        
        UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        titleButton.frame = CGRectMake(SCREEN_WIDTH/3.f*i, 0, SCREEN_WIDTH/3.f, 39);
        
        if (i==0) {
            [titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }else{
            [titleButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
        
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
        
        titleButton.tag = Title_Tag+i;
        
        [titleButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.titleScrollView addSubview:titleButton];
        
        [self.buttons addObject:titleButton];
    }
}
- (void)buttonAction:(UIButton *)button{
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    if (button.tag==0+Title_Tag) {
        [self.buttons[2] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        self.label1.backgroundColor = [UIColor blackColor];
        self.label2.backgroundColor = [UIColor whiteColor];
        self.label3.backgroundColor = [UIColor whiteColor];
        
    }else if (button.tag==1+Title_Tag){
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[2] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        self.label2.backgroundColor = [UIColor blackColor];
        self.label1.backgroundColor = [UIColor whiteColor];
        self.label3.backgroundColor = [UIColor whiteColor];
        
    }else if (button.tag==2+Title_Tag){
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        self.label3.backgroundColor = [UIColor blackColor];
        self.label2.backgroundColor = [UIColor whiteColor];
        self.label1.backgroundColor = [UIColor whiteColor];
    }
    [self setUpOneChildViewController:button.tag-Title_Tag];
    CGPoint point = self.titleScrollView.contentOffset ;
    point.x = SCREEN_WIDTH*(button.tag-Title_Tag);
    [self.contentScrollView setContentOffset:point animated:NO];
}
- (void)setUpOneChildViewController:(NSUInteger)i
{
    
    UIViewController *vc = self.childViewControllers[i];
    
    if (vc.view.superview) {
        return;
    }
    vc.view.frame = CGRectMake(i*SCREEN_WIDTH, 0,SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.titleScrollView.frame));
    
    [self.contentScrollView addSubview:vc.view];
    
}
- (NSMutableArray *)buttons{
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}
- (NSMutableArray *)labels{
    if (_labels==nil) {
        _labels = [NSMutableArray array];
    }
    return _labels;
}

#pragma mark 滚动条的懒加载
- (UILabel *)label1{
    if (_label1==nil) {
        _label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/3.f, 1)];
        _label1.backgroundColor = [UIColor blackColor];
    }
    return _label1;
}
- (UILabel *)label2{
    if (_label2==nil) {
        _label2 = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f, 0, SCREEN_WIDTH/3.f, 1)];
        _label2.backgroundColor = [UIColor whiteColor];
    }
    return _label2;
}
- (UILabel *)label3{
    if (_label3==nil) {
        _label3 = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f*2, 0, SCREEN_WIDTH/3.f, 1)];
        _label3.backgroundColor = [UIColor whiteColor];
    }
    return _label3;
}

#pragma mark contentScrollView
- (UIScrollView *)contentScrollView{
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.HScrollView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.HScrollView.frame)-64)];
        
        _contentScrollView.pagingEnabled = YES;
        _contentScrollView.delegate = self;
        
        _contentScrollView.contentOffset = CGPointMake(0, CGRectGetMaxY(self.titleScrollView.frame)+1);
        _contentScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*3, CGRectGetMaxY(self.titleScrollView.frame)+1);
    }
    return _contentScrollView;
}
#pragma markUIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger tage = self.contentScrollView.contentOffset.x/SCREEN_WIDTH;
    [self buttonAction:self.buttons[tage]];
    [self.buttons[tage] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (tage==0) {
        [self.buttons[2] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        self.label1.backgroundColor = [UIColor blackColor];
        self.label2.backgroundColor = [UIColor whiteColor];
        self.label3.backgroundColor = [UIColor whiteColor];
        
    }else if (tage==1){
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[2] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        self.label2.backgroundColor = [UIColor blackColor];
        self.label1.backgroundColor = [UIColor whiteColor];
        self.label3.backgroundColor = [UIColor whiteColor];
        
    }else if (tage==2){
        [self.buttons[0] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.buttons[1] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        self.label3.backgroundColor = [UIColor blackColor];
        self.label2.backgroundColor = [UIColor whiteColor];
        self.label1.backgroundColor = [UIColor whiteColor];
        
    }
    [self setUpOneChildViewController:tage];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [self setUpOneChildViewController:0];
}
@end
