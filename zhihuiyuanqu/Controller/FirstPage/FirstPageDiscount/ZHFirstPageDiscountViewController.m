//
//  ZHFirstPageDiscountViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageDiscountViewController.h"

#import "ZHFirstPageTableViewCell.h"

#import "ZHDiscountDetailViewController.h"

#define button_Tag 100
@interface ZHFirstPageDiscountViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *classButton;//分类按钮
@property (weak, nonatomic) IBOutlet UIButton *sortButton;//智能排序按钮
@property (weak, nonatomic) IBOutlet UIButton *nearbyButton;//附近按钮

@property(nonatomic,strong)UIScrollView *scrollView;//分类视图
@property(nonatomic,strong)UIScrollView *sortScrollView;//智能排序视图
@property(nonatomic,strong)UIScrollView *nearbyScrollView;//附近视图

@property(nonatomic,strong)NSMutableArray *classSourcedata;//分类数据源
@property(nonatomic,strong)NSMutableArray *sortSourcedata;//智能排序数据源
@property(nonatomic,strong)NSMutableArray *nearbySourcedata;//附近数据源

@property(nonatomic,strong)UITableView *tableView;

@end

@implementation ZHFirstPageDiscountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view addSubview:self.tableView];
    
    [self requestData];

}
- (void)requestData{
    [self stopRefresh];
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
}
#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    
    self.tableView.mj_header = header;
    
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHFirstPageTableViewCell" owner:self options:nil].lastObject;
    }
    
    cell.titleLabel.text = @"11";
    cell.icon.image = [UIImage imageNamed:@"1"];
    cell.titleLabel.text = @"5";
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_WIDTH/3.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHDiscountDetailViewController *detail = [[ZHDiscountDetailViewController alloc]init];
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, SCREEN_HEIGHT-30) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHFirstPageTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
#pragma mark 点击三种选项
- (IBAction)classButtonAction:(id)sender {
    
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f)];
        self.scrollView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:self.scrollView];
        for (int i=0; i<self.classSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,30*i, SCREEN_WIDTH/3.f, 30);
            [button setTitle:self.classSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(classButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }
    
}

- (void)classButAction:(UIButton *)button{
    
    [self.classButton setTitle:self.classSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.classButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.classButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}

- (IBAction)sortButtonAction:(id)sender {
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f, 30, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f)];
        self.scrollView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:self.scrollView];
        for (int i=0; i<self.sortSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,30*i, SCREEN_WIDTH/3.f, 30);
            [button setTitle:self.sortSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(sortButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }

}
- (void)sortButAction:(UIButton *)button{
    [self.sortButton setTitle:self.sortSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.sortButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.sortButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}

- (IBAction)nearbyButtonAction:(id)sender {
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f*2, 30, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f)];
        self.scrollView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:self.scrollView];
        for (int i=0; i<self.nearbySourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,30*i, SCREEN_WIDTH/3.f, 30);
            [button setTitle:self.nearbySourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(nearbyButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }
}
- (void)nearbyButAction:(UIButton *)button{
    [self.nearbyButton setTitle:self.nearbySourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.nearbyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.nearbyButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}
#pragma mark 数据源懒加载
- (NSMutableArray *)classSourcedata{
    if (_classSourcedata==nil) {
        _classSourcedata = @[@"美食",@"美容",@"宠物",@"食品",@"食品",@"食品",@"食品",@"食品",@"食品",@"食品",@"品品",@"食食"];
    }
    return _classSourcedata;
}
- (NSMutableArray *)sortSourcedata{
    if (_sortSourcedata==nil) {
        _sortSourcedata = @[@"美食",@"美容",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _sortSourcedata;
}
- (NSMutableArray *)nearbySourcedata{
    if (_nearbySourcedata==nil) {
        _nearbySourcedata = @[@"美食",@"",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _nearbySourcedata;
}
@end
