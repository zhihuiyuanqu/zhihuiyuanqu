//
//  ZHDiscountDetailViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHDiscountDetailViewController.h"

#import "ZHDiscountDetailTableViewCell.h"

#import "ZHFirstPageAdsModel.h"
@interface ZHDiscountDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation ZHDiscountDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"折扣详情";
    
    [self setNavgationItem];
    
    [self.view addSubview:self.tableView];
}

#pragma mark 导航栏设置
- (void)setNavgationItem{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(leftItemAction)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
}
- (void)leftItemAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightItemAction{
    
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHDiscountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHDiscountDetailTableViewCell"];
    if (cell==nil) {
        
        
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHDiscountDetailTableViewCell" owner:self options:nil].lastObject;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHDiscountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHDiscountDetailTableViewCell"];
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHDiscountDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZHDiscountDetailTableViewCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = @[@""];
    }
    return _dataSource;
}
@end
