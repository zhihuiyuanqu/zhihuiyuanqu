//
//  ZHFirstPageTopicViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageTopicViewController.h"

#import "ZHTopicTableViewCell.h"

#import "ZHFirstPageAdsModel.h"

#import "ZHNeighborJoinViewController.h"
@interface ZHFirstPageTopicViewController ()<UITableViewDelegate,UITableViewDataSource,ZHTopicTableViewCellDelegate>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation ZHFirstPageTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];

    [self requestData];
}
- (void)requestData{
    [self stopRefresh];
}

#pragma mark 刷新
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
}
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    
    self.tableView.mj_header = header;
    
}
#pragma mark cell的协议方法
- (void)tableViewCell:(ZHTopicTableViewCell *)cell didClickedLinkWithData:(id)data{
    
    
    
    ZHNeighborJoinViewController *join = [[ZHNeighborJoinViewController alloc]init];
    [self.navigationController pushViewController:join animated:YES];
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHTopicTableViewCell" owner:self options:nil].lastObject;
    }
    
    cell.delegate = self;
    ZHFirstPageAdsModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_WIDTH/1.3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-50-64) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHTopicTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = @[@"",@""];
    }
    return _dataSource;
}

@end
