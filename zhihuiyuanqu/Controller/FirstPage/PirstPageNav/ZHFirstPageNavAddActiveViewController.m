//
//  ZHFirstPageNavAddActiveViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageNavAddActiveViewController.h"

#import "ZHReadilyViewController.h"

@interface ZHFirstPageNavAddActiveViewController ()

@end

@implementation ZHFirstPageNavAddActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    
    self.view.backgroundColor = [UIColor lightGrayColor];
}
- (IBAction)readilyButton:(id)sender {
    ZHReadilyViewController *readily = [[ZHReadilyViewController alloc]init];
    [self.navigationController pushViewController:readily animated:YES];
}
- (IBAction)activeButton:(id)sender {
}
- (IBAction)neighborButton:(id)sender {
}
- (IBAction)backButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
