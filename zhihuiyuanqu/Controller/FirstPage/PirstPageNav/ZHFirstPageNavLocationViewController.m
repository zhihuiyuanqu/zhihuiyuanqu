//
//  ZHFirstPageNavLocationViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageNavLocationViewController.h"
#import "ZHLocationInstallViewController.h"
@interface ZHFirstPageNavLocationViewController ()<ZHLocationInstallViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *defauleLabel;

@end

@implementation ZHFirstPageNavLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)defaultPart:(NSString *)name{
    self.defauleLabel.text = name;
}
- (IBAction)setAction:(id)sender {
    
    UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
    
    ZHLocationInstallViewController *install = [story instantiateViewControllerWithIdentifier:@"ZHLocationInstallViewController"];
    
    install.delegate = self;
    
    [self.navigationController pushViewController:install animated:YES];
    
}

//点击返回主页面
- (IBAction)backButton:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}




@end
