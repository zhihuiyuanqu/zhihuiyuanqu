//
//  ZHAddPlaceTableViewCell.h
//  
//
//  Created by leo on 16/4/6.
//
//

#import <UIKit/UIKit.h>

@interface ZHAddPlaceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
