//
//  ZHLocationInstallViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLocationInstallViewController.h"

@interface ZHLocationInstallViewController ()
@property (weak, nonatomic) IBOutlet UILabel *defaultLabel;//默认小区
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdButton;
@property (weak, nonatomic) IBOutlet UIButton *fourButton;

@end

@implementation ZHLocationInstallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
- (IBAction)firstButtonAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected =NO;
    }else{
        sender.selected =YES;
        
        self.secondButton.selected = NO;
        self.thirdButton.selected = NO;
        self.fourButton.selected = NO;
        
        self.defaultLabel.text = self.firstLabel.text;
    }
    
}
- (IBAction)secondButtonAction:(UIButton *)sender {

    if (sender.selected) {
        sender.selected =NO;
    }else{
        sender.selected =YES;
        
        self.firstButton.selected = NO;
        self.thirdButton.selected = NO;
        self.fourButton.selected = NO;
        
        self.defaultLabel.text = self.secondLabel.text;
    }
}
- (IBAction)thirdButtonAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected =NO;
    }else{
        sender.selected =YES;
        
        self.secondButton.selected = NO;
        self.firstButton.selected = NO;
        self.fourButton.selected = NO;
        
        self.defaultLabel.text = self.thirdLabel.text;
    }
}
- (IBAction)fourButtonAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected =NO;
    }else{
        sender.selected =YES;
        
        self.secondButton.selected = NO;
        self.thirdButton.selected = NO;
        self.firstButton.selected = NO;
        
        self.defaultLabel.text = self.fourLabel.text;
    }
}



- (IBAction)saveAction:(id)sender {
    
    [self showAlert:@"提示" message:@"确定要保存并退出？" click:^(BOOL ret) {
        if (ret) {
            if (self.delegate) {
                [self.delegate defaultPart:self.defaultLabel.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSLog(@"取消");
        }
    }];
    
}








@end
