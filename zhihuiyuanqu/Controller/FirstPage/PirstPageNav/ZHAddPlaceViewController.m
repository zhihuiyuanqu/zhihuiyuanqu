//
//  ZHAddPlaceViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHAddPlaceViewController.h"

#import "ZHAddPlaceTableViewCell.h"

@interface ZHAddPlaceViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSArray *dataSource;

@property(nonatomic,strong)UISearchBar *search;
@end

@implementation ZHAddPlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //在导航栏上添加搜索栏
    [self addSearchBar];
    
    [self.view addSubview:self.tableView];
}

//点击返回到选择小区页面
- (IBAction)backAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.search removeFromSuperview];
}

- (IBAction)saveButton:(id)sender {
    
    [self showAlert:@"提示" message:@"确定要保存并退出?" click:^(BOOL ret) {
        if (ret) {
            NSLog(@"是");
            [self.navigationController popToRootViewControllerAnimated:YES];
            [self.search removeFromSuperview];

        }else{
            NSLog(@"否");
        }
    }];
}

- (void)addSearchBar{
    self.search = [[UISearchBar alloc]initWithFrame:CGRectMake(100, 14, SCREEN_WIDTH-200, 50)];
    self.search.showsCancelButton = NO;
    self.search.searchBarStyle = UISearchBarStyleMinimal;
    self.search.backgroundColor = [UIColor lightGrayColor];
    [self.navigationController.view addSubview:self.search];
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHAddPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[ZHAddPlaceTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.titleLabel.text = self.dataSource[indexPath.row];
    NSLog(@"%@",cell.titleLabel.text);
    NSLog(@"%@",self.dataSource[0]);
    return cell;
    
    //用xib写的cell没有显示在tableView上
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHAddPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.icon.image = [UIImage imageNamed:@"2"];
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];

        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHAddPlaceTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSArray *)dataSource{
    return @[@"小区1",@"小区2",@"小区3"];
}
@end
