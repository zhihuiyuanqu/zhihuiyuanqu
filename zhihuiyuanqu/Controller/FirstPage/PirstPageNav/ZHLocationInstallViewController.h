//
//  ZHLocationInstallViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseViewController.h"

@protocol ZHLocationInstallViewControllerDelegate <NSObject>

- (void)defaultPart:(NSString *)name;

@end
@interface ZHLocationInstallViewController : ZHBaseViewController

@property(nonatomic,weak)id<ZHLocationInstallViewControllerDelegate>delegate;
@end
