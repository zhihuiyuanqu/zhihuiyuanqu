//
//  ZHReadilyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHReadilyViewController.h"

@interface ZHReadilyViewController ()

@property(nonatomic,strong)UIScrollView *scrollView;

@property(nonatomic,strong)UITextView *textView;
@property(nonatomic,strong)UIButton *button;
@end

@implementation ZHReadilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"随手拍";
    
    [self setNavgationItem];
    
    [self.view addSubview:self.scrollView];
}

- (UITextView *)textView{
    if (_textView==nil) {
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 250)];
        _textView.backgroundColor = [UIColor redColor];
    }
    return _textView;
}
- (UIButton *)button{
    if (_button==nil) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.frame = CGRectMake(10, CGRectGetMaxY(self.textView.frame)+10, 100, 100);
        [_button setImage:[UIImage imageNamed:@"1"] forState:UIControlStateNormal];
        [_button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
- (void)buttonAction{
    
}
#pragma scrollView的懒加载
- (UIScrollView *)scrollView{
    if (_scrollView==nil) {
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        _scrollView.contentOffset = CGPointMake(0, 0);
        _scrollView.pagingEnabled = YES;
        
        [_scrollView addSubview:self.textView];
        [_scrollView addSubview:self.button];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.button.frame));
    }
    return _scrollView;
}
#pragma mark 设置导航栏
- (void)setNavgationItem{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(rightItem)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"1"] style:UIBarButtonItemStyleDone target:self action:@selector(leftItem)];
}
- (void)rightItem{
    
}
- (void)leftItem{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.hidden = YES;
}


@end
