//
//  ZHBaseViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHBaseViewController : UIViewController

- (void)showAlert:(NSString *)title message:(NSString *)message click:(void (^)(BOOL ret))ret;

@end
