//
//  ZHFirstPageAdsModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHFirstPageAdsModel : ZHBaseModel
// 图片地址
@property (nonatomic, copy) NSString *image;
@end
