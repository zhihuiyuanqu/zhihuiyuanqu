//
//  ZHFirstPageCommonModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHFirstPageCommonModel : ZHBaseModel

// 功能名称
@property (nonatomic, copy) NSString *text;

// 功能图标
@property (nonatomic, copy) NSString *image;
@end
